package com.glarimy.spark

import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD.rddToPairRDDFunctions

object HelloWorld {
  def main(args: Array[String]) {
    val sc = new SparkContext(new SparkConf().setMaster("local").setAppName("HelloWorld"))

    val content = sc.textFile("/Users/glarimy/Professional/Engineering/glarimy-spark/course.txt")
    val words = content.flatMap(line => line.split(" "))
    println("Printing the count")
    println(words.count())
    println("Printed the count")
    sc.stop
  }
}