package com.glarimy.spark

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{ IntegerType, StringType, BooleanType, StructField, StructType }

object JSONStream {

  def main(args: Array[String]) {

    val spark: SparkSession = SparkSession.builder()
      .master("local")
      .appName("marks-analyzer")
      .getOrCreate()

    val schema = StructType(
      List(
        StructField("RegdNumber", StringType, true),
        StructField("Name", StringType, true),
        StructField("Language", IntegerType, true),
        StructField("Science", IntegerType, true),
        StructField("Maths", IntegerType, true),
        StructField("Result", BooleanType, true)))

    val df = spark.readStream
      .schema(schema)
      .csv("/Users/glarimy/Professional/Engineering/glarimy-spark/marks")

    val names = df.select("Name")

    names.writeStream
      .format("console")
      .outputMode("append")
      .start()
      .awaitTermination()
  }
}