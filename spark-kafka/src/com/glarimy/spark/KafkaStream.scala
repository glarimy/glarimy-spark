package com.glarimy.spark

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer

import org.apache.spark.SparkConf
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka010._

/*
 * Add the following libraries into the classpath
 * kafka_2.11-2.4.0.jar
 * kafka-clients-2.4.0.jar
 * spark-streaming-kafka-0-10_2.11-2.4.4.jar
 * 
 */

object KafkaStream {

  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setMaster("local").setAppName("kafka")
    val ssc = new StreamingContext(sparkConf, Seconds(2))
    val topicsSet = "tweets".split(",").toSet

    val kafkaParams = Map[String, Object](
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> "localhost:9092",
      ConsumerConfig.GROUP_ID_CONFIG -> "glarimy",
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer],
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer])

    val messages = KafkaUtils.createDirectStream[String, String](
        ssc, 
        LocationStrategies.PreferConsistent, 
        ConsumerStrategies.Subscribe[String, String](topicsSet, kafkaParams))

    val lines = messages.map(_.value)
    val words = lines.flatMap(_.split(" "))
    val wordCounts = words.map(x => (x, 1L)).reduceByKey(_ + _)
    
    wordCounts.print()
    
    ssc.start()
    ssc.awaitTermination()
  }
}
